# WebRTC Build for Android

This is a vanilla WebRTC build with Java bindings packaged for Android. No additional patches have been applied.

## Use in Android App

This package is available on Maven Central (starting with version 108.0.0). It
includes the WebRTC PeerConnection build for ARM and x86, both 32 and 64 bit
builds.

Gradle:

```groovy
implementation 'im.conversations.webrtc:webrtc-android:119.0.0'
```

Maven:

```xml
<dependency>
  <groupId>im.conversations.webrtc</groupId>
  <artifactId>webrtc-android</artifactId>
  <version>119.0.0</version>
  <type>pom</type>
</dependency>
```

## How To Build

### Preparation

Figure out what version of libwebrtc you want to compile. Check the [discuss-webrtc](https://groups.google.com/g/discuss-webrtc) Google Group for recent release notes. Release notes will link to a branch head. See for example the [Release Notes for WebRTC M117](https://groups.google.com/g/discuss-webrtc/c/GKl4zc5kTog/m/_QzhHW3GAwAJ). As an alternative you can check out the [Chromium Dashboard](https://chromiumdash.appspot.com/releases?platform=Android). The third number in the version will correspond to a branch head.

### Compile libwebrtc
Rent a fast VM that runs Docker. I use [Hetzner Cloud](https://www.hetzner.com/cloud) with the Docker CE app. On the VM use [Threema’s libwebrtc Build Script](https://github.com/threema-ch/webrtc-build-docker) to compile libwebrtc.
```shell
git clone https://github.com/threema-ch/webrtc-build-docker.git
cd webrtc-build-docker
./cli.sh build-tools
./build-final.sh branch-heads/6045
tar cvf /tmp/libwebrtc.tar out/
```

### Compile Android library
On your local machine fetch the tar archive you've just created
```shell
scp vm:/tmp/libwebrtc.tar .
tar xvf libwebrtc.tar
./copy-webrtc.sh out
```

Now compile and publish the Android library.
```
./gradlew clean assemble publish
```

## Local Testing

Create a local publication (usually at `$HOME/.m2/repository/`):

    ./gradlew publishToMavenLocal

Include it in your project like this:

    repositories {
        ...
        mavenLocal()
    }


## License

    Copyright (c) 2019-2022 Threema GmbH
    Copyright (c) 2022 Danilo Bargen
    Copyright (c) 2022-2023 Daniel Gultsch

    Licensed under the Apache License, Version 2.0, <see LICENSE-APACHE file>
    or the MIT license <see LICENSE-MIT file>, at your option. This file may not be
    copied, modified, or distributed except according to those terms.
